package subiectul2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class Controller {
    private View v1;
    private int n = 0;

    public Controller(View v) {
        this.v1 = v;
        v1.setActionListener(new ChangeActionListener());
    }

    private class ChangeActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //generez un numar random intre 0 si 1000
            Random rand = new Random();
            int random = rand.nextInt(1000);
            //v1.getC1().setText(String.valueOf(random));
            v1.getC1().append(String.valueOf(random));
            v1.getC1().append("\n");
        }
    }

}