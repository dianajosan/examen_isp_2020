package subiectul2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;

public class View {
    private JFrame frame;
    private JPanel panel1;
    private JTextArea camp;
    private JButton buton;

    public View() {
        frame = new JFrame("EXAMEN");
        frame.setPreferredSize(new Dimension(1000, 1000));
        frame.setLocation(200, 100);

        panel1 = new JPanel();
        SpringLayout slPanel = new SpringLayout();
        panel1.setLayout(slPanel);

        Color color2 = new Color(0, 176, 178);
        Color color3 = new Color(0, 66, 54);


        panel1.setBackground(color2);

        camp = new JTextArea(25, 20);
        camp.setFont(new Font("Arial",1,20));
        slPanel.putConstraint(SpringLayout.NORTH, camp, 30, SpringLayout.NORTH, panel1);
        slPanel.putConstraint(SpringLayout.WEST, camp, 200, SpringLayout.WEST, panel1);
        panel1.add(camp);

        buton = new JButton("Add!");
        buton.setBackground(color3);
        buton.setForeground(color2);
        buton.setPreferredSize(new Dimension(150, 50));
        buton.setFont(new Font("Arial", Font.BOLD, 20));
        slPanel.putConstraint(SpringLayout.NORTH, buton, 700, SpringLayout.NORTH, panel1);
        slPanel.putConstraint(SpringLayout.WEST, buton, 200, SpringLayout.WEST, panel1);
        panel1.add(buton);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(0, 1));
        mainPanel.add(panel1);

        frame.setContentPane(mainPanel);
        frame.setVisible(true);
        frame.pack();

    }
    public String getCamp() {
        return camp.getText();
    }


    public JTextArea getC1() {
        return camp;
    }

    public void setCamp(String s) {
        this.camp.setText(s);
    }

    public void setActionListener(ActionListener a) {
        buton.addActionListener(a);
    }

}
