package josan.diana.examen.subiectul2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

public class View {
    private JFrame frame;
    private JPanel panel1;
    private JTextField camp1;
    private JTextField camp2;
    private JButton buton;

    public View() {
        frame = new JFrame("EXAMEN");
        frame.setPreferredSize(new Dimension(1000, 1000));
        frame.setLocation(200, 100);

        panel1 = new JPanel();
        SpringLayout slPanel = new SpringLayout();
        panel1.setLayout(slPanel);

        Color color2 = new Color(0, 176, 178);
        Color color3 = new Color(0, 66, 54);


        panel1.setBackground(color2);

        camp1 = new JTextField(30);
        camp1.setFont(new Font("Arial",1,20));
        slPanel.putConstraint(SpringLayout.NORTH, camp1 , 30, SpringLayout.NORTH, panel1);
        slPanel.putConstraint(SpringLayout.WEST, camp1 , 200, SpringLayout.WEST, panel1);
        panel1.add(camp1 );

        camp2  = new JTextField(30);
        camp2 .setFont(new Font("Arial",1,20));
        slPanel.putConstraint(SpringLayout.NORTH, camp2, 80, SpringLayout.NORTH, panel1);
        slPanel.putConstraint(SpringLayout.WEST, camp2, 200, SpringLayout.WEST, panel1);
        panel1.add(camp2);

        buton = new JButton("Change");
        buton.setBackground(color3);
        buton.setForeground(color2);
        buton.setPreferredSize(new Dimension(150, 50));
        buton.setFont(new Font("Arial", Font.BOLD, 20));
        slPanel.putConstraint(SpringLayout.NORTH, buton, 130, SpringLayout.NORTH, panel1);
        slPanel.putConstraint(SpringLayout.WEST, buton, 200, SpringLayout.WEST, panel1);
        panel1.add(buton);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(0, 1));
        mainPanel.add(panel1);

        frame.setContentPane(mainPanel);
        frame.setVisible(true);
        frame.pack();

    }
    public String getCamp1() {
        return camp1.getText();
    }

    public String getCamp2() {
        return camp2.getText();
    }
    public JTextField getC1(){
        return camp1;
    }
    public JTextField getC2(){
        return camp2;
    }

    public void setCamp1(String s) {
        this.camp1.setText(s);
    }

    public void setCamp2(String s) {
        this.camp2.setText(s);
    }

    public void setActionListener(ActionListener a) {
        buton.addActionListener(a);
    }

}