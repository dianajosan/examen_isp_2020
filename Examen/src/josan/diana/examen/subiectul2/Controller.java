package josan.diana.examen.subiectul2;

import josan.diana.examen.subiectul2.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
    private View v1;
    private int n = 0;

    public Controller(View v) {
        this.v1 = v;
        v1.setActionListener(new ChangeActionListener());
    }

    private class ChangeActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(n % 2 == 0) {
                v1.getC2().setText("");
                v1.getC1().setText("ISP");
            }else {
                v1.getC1().setText("");
                v1.getC2().setText("ISP");
            }
            n++;
        }
    }


}
